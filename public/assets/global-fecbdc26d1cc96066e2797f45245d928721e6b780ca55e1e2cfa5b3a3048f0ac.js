$('.datepicker').datepicker({
	format: "dd/mm/yyyy",
	autoclose: true,
	todayHighlight: true
});	

$('body').on("keydown", ".only-numeric", function(e){
	// Allow: backspace, delete, tab, escape, enter and .
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		// Allow: Ctrl+A
		(e.keyCode == 65 && e.ctrlKey === true) || 
		// Allow: home, end, left, right
		(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
	}
	// Ensure that it is a number and stop the keypress
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});

function readURLGalleryCreateUser(input){
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function(e){
		 $(".img_prev").remove();
			$(".foto_prev").html('<img class="img_prev" src="'+e.target.result+'" width="210px"/>');
		};
		reader.readAsDataURL(input.files[0])
	}
}

function StandarMoney(money){
	return accounting.formatMoney(money, "", ",", ".", 0)
}

function SimbolMoney(money){
	return accounting.formatMoney(money, "Rp. ", ",", ".", 0) + ",-"
}

function OriginalMoney(money){
	return parseInt(money.replace(/\.|,|,-|r|p|R|P| /g,"") || 0) 
}
