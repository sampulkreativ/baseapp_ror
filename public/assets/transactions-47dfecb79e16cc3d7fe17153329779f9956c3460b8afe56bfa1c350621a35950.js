$(document).ready(function(){
	$('body').on("keyup", "#paid", function(){
		var pay = OriginalMoney($("#payment_totals").text());
		var jml = parseInt($(this).val()); 
		var kmbl     = jml - pay;
		$(".kembalian").val(SimbolMoney(kmbl));
	});	

	$(".filters").click(function(){
		improveWait.shoWait();
		$.ajax({
			type: "GET",
			url: "/transactions",
			dataType: "script",
			data:{
				"number" : $("#number").val()
			}
		});
	})

	$(".reset").click(function(){
		$("#number").val(null);
		$.ajax({
			type: "GET",
			url: "/transactions",
			dataType: "script",
			data:{
				"number" : $("#number").val()
			}
		});
	})

	autocomplete_code();
})

function autocomplete_code(){
	var substringMatcher = function(strs) {
		return function findMatches(q, cb) {
			var matches, substringRegex;
			matches = [];
			substrRegex = new RegExp(q, 'i');
			$.each(strs, function(i, str) {
				if (substrRegex.test(str)) {
					matches.push(str);
				}
			});
			cb(matches);
		};
	};

	var arr_zip = [];
	$("#transaction_item_id").typeahead({
		hint: false,
		highlight: true,
		minLength: 1

	},
	{
		limit: 50,
		async: true,
		templates: {notFound:"Data not found"},
		source: function (query, processSync, processAsync) {
			return $.ajax({
				url: '/transactions/get_code',
				type: 'GET',
				data: {"code": $("#transaction_item_id").val()},
				dataType: 'json',
				success: function (json) {
					var _tmp_arr = [];
					json.map(function(item){
						_tmp_arr.push(item.code)
						//arr_zip.push({loc_id: item.id, st: item.state})
					})
					return processAsync(_tmp_arr);
				}
			});
		}
	})

	/*("#state").on('typeahead:selected', function (e, code) {
		arr1.map(function(i){
			if (i.st == code){
				$("#location_id").val(i.id);
			}
		})

		if(e.keyCode==13){
			arr1.map(function(i){
				if (i.st == code){
					$("#location_id").val(i.id);
				}
			})
		}
	})*/
}
