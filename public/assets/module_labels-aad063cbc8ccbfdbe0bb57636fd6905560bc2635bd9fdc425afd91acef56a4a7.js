function view_module (id) {
	$.ajax({
		type: "GET",
		url: "/module_labels/"+id,
		dataType: "script"
	});
}

function edit_module (id) {
	$.ajax({
		type: "GET",
		url: "/module_labels/"+id+"/edit",
		dataType: "script"
	});
}

function new_module () {
	$.ajax({
		type: "GET",
		url: "/module_labels/new",
		dataType: "script"
	});
}

$(".filters").click(function(){
	improveWait.shoWait();
	$.ajax({
		type: "GET",
		url: "/module_labels/filter",
		dataType: "script",
		data:{
			"name" : $("#name").val(),
			"path" : $("#path").val()
		}
	});
})

$(".reset").click(function(){
	$("#name").val(null);
	$("#path").val(null);
	$.ajax({
		type: "GET",
		url: "/module_labels",
		dataType: "script",
		data:{
			"name" : $("#name").val(),
			"path" : $("#path").val()
		}
	});
})
