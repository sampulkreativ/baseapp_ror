class RoleModule < ActiveRecord::Base
	belongs_to :role
	belongs_to :module_application

	def self.update_per_role(params)
		roles = self.where(role_id: params[:id])
		unless roles.blank?
			roles.each do |query|
				module_application = params[query.module_application.module_label.name.tr(" ","").underscore]
				unless module_application.blank?
					if query.module_application_id == module_application[:module_application_id].to_i
						query.update_attributes(
							is_read: params[query.module_application.module_label.name.tr(" ","").underscore].blank? ? false : params[query.module_application.module_label.name.tr(" ","").underscore][:is_read] == "on" ? true : false,
							is_add: params[query.module_application.module_label.name.tr(" ","").underscore].blank? ? false : params[query.module_application.module_label.name.tr(" ","").underscore][:is_add] == "on" ? true : false,
							is_update: params[query.module_application.module_label.name.tr(" ","").underscore].blank? ? false : params[query.module_application.module_label.name.tr(" ","").underscore][:is_update] == "on" ? true : false,
							is_delete: params[query.module_application.module_label.name.tr(" ","").underscore].blank? ? false : params[query.module_application.module_label.name.tr(" ","").underscore][:is_delete] == "on" ? true : false
							)
					end
				end
			end
			unless params[:module_label].blank?
				params[:module_label].each do |module_label|
					ModuleLabel.find(module_label[1][:id]).update_attributes(name: module_label[1][:name])
				end
			end
		end
	end
end
