class Traveling < ActiveRecord::Base
	has_many :tourisms, dependent: :destroy

	def self.filter(params)
 		condition = []
  		unless params[:name].blank?
  			condition << "LOWER(name) LIKE '%#{params[:name].downcase}%'"
  		end

  		conditions = condition.join(" AND ")

  		query = self.where(conditions)
  		return query
 	end
end
