class ModuleLabel < ActiveRecord::Base
	has_many :module_applications, dependent: :destroy

	#validates :name, :index_module, :path_module, :active_class, :model_class, presence: true	
	def self.filter(params)
		condition = []
  		unless params[:name].blank?
  			condition << "LOWER(name) LIKE '%#{params[:name].downcase}%'"
  		end

  		unless params[:path].blank?
  			condition << "LOWER(path_module) LIKE '%#{params[:path].downcase}%'"
  		end

  		conditions = condition.join(" AND ")

  		query = self.where(conditions)
  		return query
	end

	def self.new_child(module_label, current_user)
		if module_label.model_class != "-"
			mod_app = ModuleApplication.new(
				module_label_id: module_label.id, 
				action_read: "can :read, #{module_label.model_class};",
				action_create: "can :create, #{module_label.model_class};",
				action_update: "can :update, #{module_label.model_class};",
				action_destroy: "can :destroy, #{module_label.model_class};",
			)
			mod_app.save
			role_module = RoleModule.create!([
					{role_id: 1, module_application_id: mod_app.id, is_read: 1, is_add: 1, is_update: 1, is_delete: 1},
					{role_id: 2, module_application_id: mod_app.id, is_read: 1, is_add: 1, is_update: 1, is_delete: 1},
					{role_id: 3, module_application_id: mod_app.id, is_read: 0, is_add: 0, is_update: 0, is_delete: 0},
					# {role_id: 4, module_application_id: mod_app.id, is_read: 0, is_add: 0, is_update: 0, is_delete: 0}
				])
		end
	end

	def self.update_child(module_label)
		mod_app = ModuleApplication.find_by_module_label_id(module_label.id)
		unless mod_app.blank?
			if module_label.model_class != "-"
				mod_app.update_attributes(
					action_read: "can :read, #{module_label.model_class};",
					action_create: "can :create, #{module_label.model_class};",
					action_update: "can :update, #{module_label.model_class};",
					action_destroy: "can :destroy, #{module_label.model_class};",
				)
			end
		end
	end
end
