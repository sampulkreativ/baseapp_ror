class Tourism < ActiveRecord::Base
  belongs_to :point
  belongs_to :traveling
	validates :answer, presence: true	

	def self.filter(params)
 		condition = []
  		unless params[:name].blank?
  			condition << "LOWER(name) LIKE '%#{params[:name].downcase}%'"
  		end

  		conditions = condition.join(" AND ")

  		query = self.where(conditions)
  		return query
 	end

  def self.save_data(data, traveling)
    questions = Question.all.order("id ASC")
    unless questions.blank?
        questions.each do |question|                  
          data[:tourism]["answer_#{question.id}"].split("|")[0].each_with_index do |x,index| 
            point_id = data[:tourism]["answer_3_#{question.id}"].blank? ? data[:tourism]["answer_#{question.id}"][index].split("|")[1].blank? ? question.points.pluck(:id)[0] : data[:tourism]["answer_#{question.id}"][0].split("|")[1].to_i : data[:tourism]["answer_3_#{question.id}"][index].split("|")[1]
            if data[:tourism]["answer_3_#{question.id}"].present?
              wheres = "point_id = #{point_id}"
            else
              wheres = "traveling_id = #{traveling.id}"
            end
            tourism = self.where(question_id: question.id).where(wheres).last || self.new            
            tourism.answer = data[:tourism]["answer_#{question.id}"][index].split("|")[0]
            tourism.point_id = point_id
            tourism.values = data[:tourism]["answer_3_#{question.id}"].blank? ? data[:tourism]["answer_#{question.id}"][index].split("|")[2].blank? ? question.points.pluck(:values)[0] : data[:tourism]["answer_#{question.id}"][0].split("|")[2].to_f : data[:tourism]["answer_3_#{question.id}"][index].split("|")[2]
            tourism.traveling_id = traveling.id
            tourism.question_id = question.id
            tourism.save        
          end
        end
        return true      
    else
      return false
    end
  end
end
