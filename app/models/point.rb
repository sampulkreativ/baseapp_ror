class Point < ActiveRecord::Base
	belongs_to :question
	has_many :tourisms, dependent: :destroy
end
