class Ability
  include CanCan::Ability

  def initialize(user)
        user.role.role_modules.each do |role_module|        	        	
            eval(role_module.module_application.action_read) if role_module.is_read == true
            eval(role_module.module_application.action_create) if role_module.is_add == true
            eval(role_module.module_application.action_update) if role_module.is_update == true
            eval(role_module.module_application.action_destroy) if role_module.is_delete == true
        end
  end
end
