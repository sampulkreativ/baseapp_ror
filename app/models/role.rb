class Role < ActiveRecord::Base
	has_many :users, dependent: :destroy
	has_many :module_applications, through: :role_modules, dependent: :destroy
	has_many :role_modules, dependent: :destroy

	validates :name, presence: true
	validates :name, uniqueness: true
	
	def self.filter(params)
 		condition = []
  		unless params[:name].blank?
  			condition << "LOWER(name) LIKE '%#{params[:name].downcase}%'"
  		end

  		conditions = condition.join(" AND ")

  		query = self.where(conditions)
  		return query
 	end
end
