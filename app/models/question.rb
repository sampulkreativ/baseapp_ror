class Question < ActiveRecord::Base
	belongs_to :type_answer
	has_many :points, dependent: :destroy
	has_many :answers, dependent: :destroy	

	accepts_nested_attributes_for :points
	def self.filter(params)
 		condition = []
  		unless params[:name].blank?
  			condition << "LOWER(name) LIKE '%#{params[:name].downcase}%'"
  		end

  		conditions = condition.join(" AND ")

  		query = self.where(conditions)
  		return query
 	end
end
