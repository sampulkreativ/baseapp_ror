class ModuleApplication < ActiveRecord::Base
	has_many :roles, through: :role_modules, dependent: :destroy
	has_many :role_modules, dependent: :destroy
	belongs_to :module_label
end
