class User < ActiveRecord::Base	
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
	belongs_to :role	
	devise :database_authenticatable, :registerable,:recoverable, :rememberable, :trackable, :validatable

	has_attached_file :photo,
	:style => {
		original: "40x40#", large: '50x50>' },
	:url => "/system/:attachment/:id/:style/:basename.:extension",
		:path => ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
		:default_url => "/assets/:style/add_user.png"
		validates_attachment_content_type :photo, :content_type => /image/

		validates :email,:full_name, :birthday, :phone, :address, presence:true

	def self.filter(params)
		condition = []
			unless params[:username].blank?
				condition << "LOWER(username) LIKE '%#{params[:username].downcase}%'"
			end

			unless params[:role].blank?
				condition << "role_id = #{params[:role]}"
			end

			unless params[:email].blank?
				condition << "LOWER(email) LIKE '%#{params[:email].downcase}%'"
			end

			conditions = condition.join(" AND ")

			query = self.where(conditions)
			return query
	end

end
