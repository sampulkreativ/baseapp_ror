module BaseHelper
	def set_menus(menus, current_user)
		html =""
		menus.each do |menu|
			html += "<li class='treeview "
			html += instance_variable_get("@#{menu.active_class}").blank? ? "" : instance_variable_get("@#{menu.active_class}")
			html += "'>"
			html += "<a href='#'><i class='#{menu.module_class}'></i>"
			html += "<span>#{menu.name}</span> <i class='fa fa-angle-left pull-right'></i>"
			html += "</a>"
			html += "<ul class='treeview-menu'>"
			mod_labels = ModuleLabel.where(child: menu.id)
			mods = ModuleApplication.where("module_label_id IN #{mod_labels.blank? ? "(0)" : mod_labels.map{|x| x.id}.to_s.tr('[]','()')}")
			unless mods.blank?
				mods.each do |mod|
					role_module = RoleModule.find_by_role_id_and_module_application_id(current_user.role_id, mod.id)
					unless role_module.blank?
						if role_module.is_read
							html += "<li class="
							html += instance_variable_get("@#{mod.module_label.active_class}").blank? ? "" : instance_variable_get("@#{mod.module_label.active_class}")
							html += ">"
							html += "<a href='#{eval(mod.module_label.path_module)}'><i class='#{mod.module_label.module_class}'></i> #{mod.module_label.name} </a>"
							html += "</li>"
						end
					end
				end
				html += "</ul>"
				html += "</li>"
			end			
		end
		html.html_safe
	end
end
