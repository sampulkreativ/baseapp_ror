module ApplicationHelper
	def numbering(counter, page_number)
		counter + 1 + (page_number.nil? || page_number == "0" || page_number == "1" ? 0 : ((page_number.to_i + 0)))
	end

	def SimbolMoney(money)
		money.to_i > 0 ? number_to_currency(money, unit: "Rp.", separator: ",", delimiter: ".", precision: 0, format: "%u %n,-") : '-'
	end
end
