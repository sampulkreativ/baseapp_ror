module CodeSequencesHelper
	def generated_example_sequence(sequence)
		html = ""
		html += sequence.prefix
		html += "-"
		html += sequence.next.to_s.rjust(sequence.digit, '0')
	end
end
