module TravelingsHelper

	def find_answer(traveling, question, point_id=nil, type_id=0)
		tourisms = Tourism.where(traveling_id: traveling.id, question_id: question.id, point_id: point_id)		
		data_answer = question.points.pluck(:id).include? tourisms.map{|x| x.point_id}.last	
		type_answer = TypeAnswer.where("LOWER(name) like '%multiple choice%'") rescue ""
		if data_answer			
			answer = [tourisms.last.answer,"<div class='col-lg-10 no-padding'>#{tourisms.last.answer}</div> <b>Point :</b> #{tourisms.last.values}".html_safe, tourisms.last.id]			
		else
			answer = [nil,"-", nil]
		end		
		return answer
	end

	def find_single_answer(traveling, question, point_id=nil, type_id=0)
		tourisms = Tourism.where(traveling_id: traveling.id, question_id: question.id, point_id: point_id)		
		data_answer = question.points.pluck(:id).include? tourisms.map{|x| x.point_id}.last	
		type_answer = TypeAnswer.where("LOWER(name) like '%multiple choice%'") rescue ""		
		if data_answer			
			if type_id == type_answer.last.id
				answer = []				
				tourisms.each_with_index do |tourism, index|				
					answer << "<div class='col-lg-10 no-padding'>#{index+1}. <b>#{tourism.point.label}</b> = #{tourisms.last.answer}</div> <b>Point :</b> #{tourism.values}<br>".html_safe
				end
			else
				answer = ["<div class='col-lg-10 no-padding'>#{tourisms.last.answer}</div> <b>Point :</b> #{tourisms.last.values}".html_safe]
			end
		else
			answer = [nil,"-", nil]
		end		
		return answer
	end
end
