module ModuleLabelsHelper	

	def find_child(module_label)
		results = ModuleLabel.where(child: module_label.id)
		html = ""
		results.each do |module_label|
			html += "<tr>"
			html += "<td>"
			html += module_label.name.to_s
			html += "</td>"
			html += "<td>"
			html += module_label.path_module.to_s
			html += "</td>"
			html += "<td>"
			html += module_label.description.to_s
			html += "</td>"
			html += "<td>"
			html += "<div class='btn-group btn-group-justified'>"
			html += "<a href='javascript:void(0)' title='Detail' class='btn btn-default' onclick='view_module(#{module_label.id})'><i class='fa fa-eye'></i></a>"
			html += "<a href='javascript:void(0)' title='Edit' class='btn btn-default' onclick='edit_module(#{module_label.id})'><i class='fa fa-pencil'></i></a>"
			html += "<a title='Hapus' class='btn btn-default' data-confirm='Are you sure?' rel='nofollow' data-method='delete' href='/module_labels/#{module_label.id}''><i class='fa fa-trash-o'></i></a>"
			html += "</div>"
			html += "</td>"
			html += "</tr>"
		end
		return html.html_safe
	end
end
