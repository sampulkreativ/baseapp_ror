module TransactionsHelper

	def find_price(item, current_user)
		ItemWarehouse.where(item_id: item, warehouse_id: current_user.warehouse_id).first.price
	end
end
