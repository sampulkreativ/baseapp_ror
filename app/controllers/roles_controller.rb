class RolesController < BaseController
	before_action :new_role, :only => [:create]
	load_and_authorize_resource
	before_action :set_role, only: [:show, :edit, :update, :destroy]
	before_action :set_sidebar
	respond_to :html

	before_action :set_page_title

	def index
		@roles = Role.filter(params).order("id ASC").page(params[:page]).per(20)
		@roles_all = Role.filter(params)
	end

	def show
	end

	def new
		@role = Role.new
	end

	def edit
	end

	def create
		#@role = Role.new(role_params)
		respond_to do |format|
			if @role.save
				format.html { redirect_to @role, notice: 'Role was successfully created.' }
				format.json { render :show, status: :created, location: @role }
			else
				format.html { render :new }
				format.json { render json: @role.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		update_role_module = RoleModule.update_per_role(params)
		unless params[:role].blank?
			role = @role.update(role_params)
		end
		respond_to do |format|
			if role
				format.html { redirect_to roles_path, notice: 'Role was successfully updated.' }
				format.json { render :show, status: :created, location: @role }
			else
				format.html { render :edit }
				format.json { render json: @role.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@role.destroy
		respond_to do |format|
			format.html { redirect_to roles_url, notice: 'Role was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		def new_role
			@role = Role.new(role_params)
		end

		def set_sidebar
			@page_title = "Setting"
			@page_description = "Role"
		end

		def set_page_title
			@open_setting = "selected active"
			@active_listing_role = "active"
		end

		def set_role
			@role = Role.find(params[:id])
			@role_modules = RoleModule.where(role_id: params[:id]).order("id ASC")
		end

		def role_params
			params.require(:role).permit(:name, :description)
		end
end
