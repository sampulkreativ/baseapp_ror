class TravelingsController < ApplicationController 
  before_action :new_traveling, :only => [:create]
  load_and_authorize_resource
  before_action :set_traveling, only: [:show, :edit, :update, :destroy]
  before_action :set_sidebar
  respond_to :html

  before_action :set_page_title

  before_action :set_questions, only: [:new, :edit, :create, :update, :show]
  # GET /travelings
  # GET /travelings.json
  def index
    @travelings = Traveling.filter(params).order("id DESC").page(params[:page]).per(20)
    @travelings_all = Traveling.filter(params)
  end

  # GET /travelings/1
  # GET /travelings/1.json
  def show
  end

  # GET /travelings/new
  def new
    @traveling = Traveling.new
  end

  # GET /travelings/1/edit
  def edit
  end

  # POST /travelings
  # POST /travelings.json
  def create
    @traveling = Traveling.new(traveling_params)    
    @traveling.save
    save = Tourism.save_data(params, @traveling)
    respond_to do |format|
      if save
        format.html { redirect_to @traveling, notice: 'Traveling was successfully created.' }
        format.json { render :show, status: :created, location: @traveling }
      else
        format.html { render :new }
        format.json { render json: @traveling.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /travelings/1
  # PATCH/PUT /travelings/1.json
  def update
    respond_to do |format|
      if @traveling.update(traveling_params)
        save = Tourism.save_data(params, @traveling)
        format.html { redirect_to @traveling, notice: 'Traveling was successfully updated.' }
        format.json { render :show, status: :ok, location: @traveling }
      else
        format.html { render :edit }
        format.json { render json: @traveling.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /travelings/1
  # DELETE /travelings/1.json
  def destroy
    @traveling.destroy
    respond_to do |format|
      format.html { redirect_to travelings_url, notice: 'Traveling was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_traveling
      @traveling = Traveling.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def traveling_params
      params.require(:traveling).permit(:name, :description)
    end

    def new_traveling
      @traveling = Traveling.new(traveling_params)
    end

    def set_sidebar
      @page_title = "Sidak"
      @page_description = "Pariwisata"
    end

    def set_page_title
      @open_sidak = "selected active"
      @active_listing_traveling = "active"
    end

    def set_questions
      @questions = Question.all.order("id ASC")
    end
end
