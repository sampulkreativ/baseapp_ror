class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	before_action :set_menus
	
	rescue_from CanCan::AccessDenied do |exception|
		redirect_to root_url, flash: {alert: "Access denied when access this page"}
	end

	def set_menus
		@menus = ModuleLabel.where(child: nil).order("id ASC")
	end
end
