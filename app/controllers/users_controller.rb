class UsersController < BaseController
	before_action :new_user, :only => [:create]
	load_and_authorize_resource
	before_action :set_user, only: [:show, :edit, :update, :destroy]
	before_action :set_sidebar
	respond_to :html

	before_action :set_page_title
	def index
		@users = User.filter(params).order("id ASC").page(params[:page]).per(20)
		@users_all = User.filter(params)
		respond_to do |format|
			format.js
			format.html
		end
	end

	def edit;end

	def new
		@user = User.new
	end

	def create
		@user.save
		respond_with(@user)
	end

	def update
		user = @user.update(user_params)
		respond_to do |format|
			format.js
			if user			
				format.html {redirect_to users_path, notice: "Updated Successfully"}
			else
				format.html {render :edit}
			end
		end
	end

	def destroy
		@user.destroy
		flash[:notice] = "Sukses menghapus data user"
		respond_with(@user)
	end

	private
	def new_user
		@user = User.new(user_params)
	end
	
	def set_user
		@user = User.find(params[:id])
	end

	def user_params
		params.require(:user).permit(:email, :password, :full_name, :role_id, :birthday, :phone, :address, :photo, :salt, :encrypted_password,  :popup, :password_confirmation)
	end

	def set_sidebar
		@page_title = "Data Master"
		@page_description = "Data User"
	end

	def set_page_title
		@open_data_master = "selected active"
		@active_user = "active"
	end
end
