class DashboardController < BaseController
	before_action :set_sidebar
	respond_to :html
	before_action :set_page_title

	#add_crumb '<i class="fa fa-dashboard"></i> Dashboard'.html_safe, '#'
	#add_crumb('<i class="fa fa-file"></i> Main Page'.html_safe, only: ["index", "new", "show", "edit", "create", "update"]) { |instance| instance.send :reports_path }

	def index;end

	private
		def set_sidebar
			@page_title = "Dashboard"
			@page_description = "Main page"
		end

		def set_page_title
			@active_listing_dashboard = "selected active"
		end
end
