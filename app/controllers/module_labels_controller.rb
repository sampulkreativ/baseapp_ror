class ModuleLabelsController < BaseController
	before_action :new_module_label, :only => [:create]
	load_and_authorize_resource except: [:filter, :create]
	before_action :set_role, only: [:show, :edit, :update, :destroy]
	before_action :set_sidebar
	respond_to :html

	before_action :set_page_title

	def index
		@modules = ModuleLabel.where(child: nil).order("id ASC")
		@modules_all = ModuleLabel.all
		@modules_single = ModuleLabel.order("id ASC").page(params[:page]).per(20)
	end

	def new
		@module = ModuleLabel.new
	end

	def create
		if @module.save
			save_child = ModuleLabel.new_child(@module, current_user)
		end
		respond_to do |format|
			format.js
		end
	end

	def show;end

	def edit;end

	def update
		@module.update(module_params)
		update_child = ModuleLabel.update_child(@module)
		respond_to do |format|
			format.js
		end
	end

	def destroy
		@module.destroy
		respond_with(@module)
	end

	def filter
		@modules_single = ModuleLabel.filter(params).order("id ASC").page(params[:page]).per(20)
		@modules_all = ModuleLabel.filter(params)
	end

	private
		def new_module_label
			params[:module_label][:active_class] = params[:module_label][:active_class].tr(" ","")
			@module = ModuleLabel.new(module_params)
		end

		def set_sidebar
			@page_title = "Setting"
			@page_description = "Module"
		end

		def set_page_title
			@open_setting = "selected active"
			@active_listing_module = "active"
		end

		def set_role
			@module = ModuleLabel.find(params[:id])
		end

		def module_params
			params.require(:module_label).permit(:name, :description, :child, :index_module, :path_module, :module_class, :active_class, :model_class)
		end
end
