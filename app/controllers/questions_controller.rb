class QuestionsController < ApplicationController 
  before_action :new_question, :only => [:create]
  load_and_authorize_resource
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  before_action :set_sidebar
  respond_to :html
  before_action :set_page_title, :set_options

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.filter(params).order("id DESC").page(params[:page]).per(20)
    @questions_all = Question.filter(params)
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/1/edit
  def edit
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)

    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        @question.points.where("id NOT IN (?)", params[:question][:points_attributes].map{|x| x[1][:id]}).delete_all
        format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questions_url, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:name, :type_answer_id, :description, points_attributes: [:values, :label])
    end

    def new_question
      @question = Question.new(question_params)
    end

    def set_sidebar
      @page_title = "Pertanyaan"
      @page_description = "Untuk Set Pertanyaan"
    end

    def set_page_title
      @open_data_master = "selected active"
      @active_listing_question = "active"
    end

    def set_options
      @type_answers = TypeAnswer.all.order("name ASC")
    end
end
