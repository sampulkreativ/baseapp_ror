$("body").on("change", "#item_code_sequence_id", function(){
	$.get("/items/get_sequence", {
		prefix_id: $(this).val(),
		dataType: "script"
	});
})

$(".filters").click(function(){
	improveWait.shoWait();
	$.ajax({
		type: "GET",
		url: "/items",
		dataType: "script",
		data:{
			"name" : $("#name").val(),
			"code" : $("#code").val(),
			"price_from" : $("#price_from").val(),
			"price_to" : $("#price_to").val()
		}
	});
})

$(".reset").click(function(){
	$("#name").val(null);
	$("#code").val(null);
	$("#price_from").val(null);
	$("#price_to").val(null);
	$.ajax({
		type: "GET",
		url: "/items",
		dataType: "script",
		data:{
			"name" : $("#name").val(),
			"code" : $("#code").val(),
			"price_from" : $("#price_from").val(),
			"price_to" : $("#price_to").val()
		}
	});
})
