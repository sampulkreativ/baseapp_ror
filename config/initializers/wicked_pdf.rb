 if Rails.env.staging? || Rails.env.production?
	#WickedPdf.config = {
	  #:wkhtmltopdf => '/usr/local/bin/wkhtmltopdf',
	  #:layout => "pdf.html",
	#  	:exe_path => Rails.root.join('bin', 'wkhtmltopdf-amd64').to_s
	#}
	WickedPdf.config ||= {}
	WickedPdf.config.merge!({
	  # your extra configurations here
	})
else
	WickedPdf.config = {
		:exe_path => '/home/wgs-lap005/.rvm/gems/ruby-2.0.0-p594/bin/wkhtmltopdf'
	}
end

Mime::Type.register "application/pdf", :pdf