Rails.application.routes.draw do 
  resources :travelings
  resources :questions
  root 'dashboard#index'
  devise_for :users, 
		:path => "portal",
		controllers: {
			sessions: 'portal/sessions',
			:registrations => "portal/registrations",
			:passwords => "portal/passwords"
		},
		:path_names => { 
			:sign_in => 'login', 
			:sign_out => 'logout'
		}
  resources :users
  resources :tourisms
  resources :roles
	resources :module_labels do
		collection do
			get :filter
		end
	end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
