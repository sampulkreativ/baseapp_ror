require "application_system_test_case"

class TravelingsTest < ApplicationSystemTestCase
  setup do
    @traveling = travelings(:one)
  end

  test "visiting the index" do
    visit travelings_url
    assert_selector "h1", text: "Travelings"
  end

  test "creating a Traveling" do
    visit travelings_url
    click_on "New Traveling"

    fill_in "Description", with: @traveling.description
    fill_in "Name", with: @traveling.name
    click_on "Create Traveling"

    assert_text "Traveling was successfully created"
    click_on "Back"
  end

  test "updating a Traveling" do
    visit travelings_url
    click_on "Edit", match: :first

    fill_in "Description", with: @traveling.description
    fill_in "Name", with: @traveling.name
    click_on "Update Traveling"

    assert_text "Traveling was successfully updated"
    click_on "Back"
  end

  test "destroying a Traveling" do
    visit travelings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Traveling was successfully destroyed"
  end
end
