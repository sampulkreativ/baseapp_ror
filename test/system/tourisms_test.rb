require "application_system_test_case"

class TourismsTest < ApplicationSystemTestCase
  setup do
    @tourism = tourisms(:one)
  end

  test "visiting the index" do
    visit tourisms_url
    assert_selector "h1", text: "Tourisms"
  end

  test "creating a Tourism" do
    visit tourisms_url
    click_on "New Tourism"

    fill_in "Odtw alamat", with: @tourism.odtw_alamat
    fill_in "Odtw desa", with: @tourism.odtw_desa
    fill_in "Odtw email", with: @tourism.odtw_email
    fill_in "Odtw gps string", with: @tourism.odtw_gps_string
    fill_in "Odtw hp", with: @tourism.odtw_hp
    fill_in "Odtw kecamatan", with: @tourism.odtw_kecamatan
    fill_in "Odtw kodepos", with: @tourism.odtw_kodepos
    fill_in "Odtw kota", with: @tourism.odtw_kota
    fill_in "Odtw nama", with: @tourism.odtw_nama
    fill_in "Odtw telp", with: @tourism.odtw_telp
    fill_in "Odtw website", with: @tourism.odtw_website
    click_on "Create Tourism"

    assert_text "Tourism was successfully created"
    click_on "Back"
  end

  test "updating a Tourism" do
    visit tourisms_url
    click_on "Edit", match: :first

    fill_in "Odtw alamat", with: @tourism.odtw_alamat
    fill_in "Odtw desa", with: @tourism.odtw_desa
    fill_in "Odtw email", with: @tourism.odtw_email
    fill_in "Odtw gps string", with: @tourism.odtw_gps_string
    fill_in "Odtw hp", with: @tourism.odtw_hp
    fill_in "Odtw kecamatan", with: @tourism.odtw_kecamatan
    fill_in "Odtw kodepos", with: @tourism.odtw_kodepos
    fill_in "Odtw kota", with: @tourism.odtw_kota
    fill_in "Odtw nama", with: @tourism.odtw_nama
    fill_in "Odtw telp", with: @tourism.odtw_telp
    fill_in "Odtw website", with: @tourism.odtw_website
    click_on "Update Tourism"

    assert_text "Tourism was successfully updated"
    click_on "Back"
  end

  test "destroying a Tourism" do
    visit tourisms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tourism was successfully destroyed"
  end
end
