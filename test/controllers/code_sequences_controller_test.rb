require 'test_helper'

class CodeSequencesControllerTest < ActionController::TestCase
  setup do
    @code_sequence = code_sequences(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:code_sequences)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create code_sequence" do
    assert_difference('CodeSequence.count') do
      post :create, code_sequence: { digit: @code_sequence.digit, name: @code_sequence.name, next: @code_sequence.next, prefix: @code_sequence.prefix }
    end

    assert_redirected_to code_sequence_path(assigns(:code_sequence))
  end

  test "should show code_sequence" do
    get :show, id: @code_sequence
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @code_sequence
    assert_response :success
  end

  test "should update code_sequence" do
    patch :update, id: @code_sequence, code_sequence: { digit: @code_sequence.digit, name: @code_sequence.name, next: @code_sequence.next, prefix: @code_sequence.prefix }
    assert_redirected_to code_sequence_path(assigns(:code_sequence))
  end

  test "should destroy code_sequence" do
    assert_difference('CodeSequence.count', -1) do
      delete :destroy, id: @code_sequence
    end

    assert_redirected_to code_sequences_path
  end
end
