require 'test_helper'

class TravelingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @traveling = travelings(:one)
  end

  test "should get index" do
    get travelings_url
    assert_response :success
  end

  test "should get new" do
    get new_traveling_url
    assert_response :success
  end

  test "should create traveling" do
    assert_difference('Traveling.count') do
      post travelings_url, params: { traveling: { description: @traveling.description, name: @traveling.name } }
    end

    assert_redirected_to traveling_url(Traveling.last)
  end

  test "should show traveling" do
    get traveling_url(@traveling)
    assert_response :success
  end

  test "should get edit" do
    get edit_traveling_url(@traveling)
    assert_response :success
  end

  test "should update traveling" do
    patch traveling_url(@traveling), params: { traveling: { description: @traveling.description, name: @traveling.name } }
    assert_redirected_to traveling_url(@traveling)
  end

  test "should destroy traveling" do
    assert_difference('Traveling.count', -1) do
      delete traveling_url(@traveling)
    end

    assert_redirected_to travelings_url
  end
end
