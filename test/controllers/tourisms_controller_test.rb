require 'test_helper'

class TourismsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tourism = tourisms(:one)
  end

  test "should get index" do
    get tourisms_url
    assert_response :success
  end

  test "should get new" do
    get new_tourism_url
    assert_response :success
  end

  test "should create tourism" do
    assert_difference('Tourism.count') do
      post tourisms_url, params: { tourism: { odtw_alamat: @tourism.odtw_alamat, odtw_desa: @tourism.odtw_desa, odtw_email: @tourism.odtw_email, odtw_gps_string: @tourism.odtw_gps_string, odtw_hp: @tourism.odtw_hp, odtw_kecamatan: @tourism.odtw_kecamatan, odtw_kodepos: @tourism.odtw_kodepos, odtw_kota: @tourism.odtw_kota, odtw_nama: @tourism.odtw_nama, odtw_telp: @tourism.odtw_telp, odtw_website: @tourism.odtw_website } }
    end

    assert_redirected_to tourism_url(Tourism.last)
  end

  test "should show tourism" do
    get tourism_url(@tourism)
    assert_response :success
  end

  test "should get edit" do
    get edit_tourism_url(@tourism)
    assert_response :success
  end

  test "should update tourism" do
    patch tourism_url(@tourism), params: { tourism: { odtw_alamat: @tourism.odtw_alamat, odtw_desa: @tourism.odtw_desa, odtw_email: @tourism.odtw_email, odtw_gps_string: @tourism.odtw_gps_string, odtw_hp: @tourism.odtw_hp, odtw_kecamatan: @tourism.odtw_kecamatan, odtw_kodepos: @tourism.odtw_kodepos, odtw_kota: @tourism.odtw_kota, odtw_nama: @tourism.odtw_nama, odtw_telp: @tourism.odtw_telp, odtw_website: @tourism.odtw_website } }
    assert_redirected_to tourism_url(@tourism)
  end

  test "should destroy tourism" do
    assert_difference('Tourism.count', -1) do
      delete tourism_url(@tourism)
    end

    assert_redirected_to tourisms_url
  end
end
