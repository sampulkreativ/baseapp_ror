class CreateRoleModules < ActiveRecord::Migration[5.2]
  def change
    create_table :role_modules do |t|
    	t.integer :role_id
    	t.integer :module_application_id
    	t.boolean :is_read, deafule: false
    	t.boolean :is_add, default: false
    	t.boolean :is_update, default: false
      t.timestamps null: false
    end
  end
end
