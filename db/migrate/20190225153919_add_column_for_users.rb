class AddColumnForUsers < ActiveRecord::Migration[5.2]
  def change
		add_column :users, :role_id, :integer
	  	add_column :users, :full_name, :string
	  	add_attachment :users, :photo
	  	add_column :users, :address, :text
	  	add_column :users, :birthday, :date
	  	add_column :users, :phone, :string
	end
end
