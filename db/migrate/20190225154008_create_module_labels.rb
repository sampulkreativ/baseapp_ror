class CreateModuleLabels < ActiveRecord::Migration[5.2]
  def change
    create_table :module_labels do |t|
    	t.string :name
    	t.text :description
    	t.integer :index_module
    	t.string :path_module
    	t.string :module_class
    	t.boolean :child, default: false
      t.timestamps null: false
    end
  end
end
