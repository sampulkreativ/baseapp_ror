class AddColumnActiveClassInModuleLabels < ActiveRecord::Migration[5.2]
  def change
  	add_column :module_labels, :active_class, :string
  end
end
