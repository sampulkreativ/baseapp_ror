class AddColumnQuestionIdInTourisms < ActiveRecord::Migration[5.2]
  def change
  	add_column :tourisms, :question_id, :integer
  end
end
