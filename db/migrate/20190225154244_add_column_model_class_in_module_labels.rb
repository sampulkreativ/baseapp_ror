class AddColumnModelClassInModuleLabels < ActiveRecord::Migration[5.2]
  def change
  	add_column :module_labels, :model_class, :string
  end
end
