class CreateTourisms < ActiveRecord::Migration[5.2]
  def change
    create_table :tourisms do |t|
      t.string :odtw_nama
      t.text :odtw_alamat
      t.string :odtw_desa
      t.string :odtw_kecamatan
      t.string :odtw_kota
      t.integer :odtw_kodepos
      t.string :odtw_telp
      t.string :odtw_hp
      t.string :odtw_email
      t.string :odtw_website
      t.string :odtw_gps_string

      t.timestamps
    end
  end
end
