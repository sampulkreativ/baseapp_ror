class AddColumnTourismIdInGrounds < ActiveRecord::Migration[5.2]
  def change
  	add_column :grounds, :tourism_id, :integer
  end
end
