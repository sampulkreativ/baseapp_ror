class RemoveFieldTourismChangeToAnswers < ActiveRecord::Migration[5.2]
  def change
  	remove_column :tourisms, :odtw_nama
  	remove_column :tourisms, :odtw_alamat
  	remove_column :tourisms, :odtw_desa
  	remove_column :tourisms, :odtw_kecamatan
  	remove_column :tourisms, :odtw_kota
  	remove_column :tourisms, :odtw_kodepos
  	remove_column :tourisms, :odtw_telp
  	remove_column :tourisms, :odtw_hp
  	remove_column :tourisms, :odtw_email
  	remove_column :tourisms, :odtw_website
  	remove_column :tourisms, :odtw_gps_string

  	add_column :tourisms, :answer, :string
  	add_column :tourisms, :point_id, :integer  	
  end
end
