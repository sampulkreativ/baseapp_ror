class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.string :name
      t.integer :type_answer_id
      t.text :description

      t.timestamps
    end
  end
end
