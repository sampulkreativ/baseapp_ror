class ChangeColumnModelClassInModuleLabels < ActiveRecord::Migration[5.2]
  def change
  	remove_column :module_labels, :child
  	add_column :module_labels, :child, :integer
  end
end
