class CreateModuleApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :module_applications do |t|
    	t.integer :module_label_id
    	t.string :action_read
		t.string :action_create
		t.string :action_update
		t.string :action_destroy
      t.timestamps null: false
    end
  end
end
