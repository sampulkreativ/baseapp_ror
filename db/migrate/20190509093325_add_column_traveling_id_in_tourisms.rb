class AddColumnTravelingIdInTourisms < ActiveRecord::Migration[5.2]
  def change
  	add_column :tourisms, :traveling_id, :integer
  end
end
