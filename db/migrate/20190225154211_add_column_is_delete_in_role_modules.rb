class AddColumnIsDeleteInRoleModules < ActiveRecord::Migration[5.2]
  def change
  	add_column :role_modules, :is_delete, :boolean, default: false
  end
end
