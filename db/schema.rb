# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_10_070321) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.string "name"
    t.boolean "visible"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grounds", force: :cascade do |t|
    t.integer "grd_luas"
    t.string "grd_negara"
    t.string "grd_kehutanan"
    t.string "grd_perkebunan"
    t.string "grd_pemerintah"
    t.string "grd_pemprov"
    t.string "grd_pemkab"
    t.string "grd_desa"
    t.string "grd_swasta"
    t.string "grd_perorangan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tourism_id"
  end

  create_table "module_applications", force: :cascade do |t|
    t.integer "module_label_id"
    t.string "action_read"
    t.string "action_create"
    t.string "action_update"
    t.string "action_destroy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "module_labels", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "index_module"
    t.string "path_module"
    t.string "module_class"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "active_class"
    t.integer "child"
    t.string "model_class"
  end

  create_table "points", force: :cascade do |t|
    t.integer "values"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "label"
  end

  create_table "questions", force: :cascade do |t|
    t.string "name"
    t.integer "type_answer_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "role_modules", force: :cascade do |t|
    t.integer "role_id"
    t.integer "module_application_id"
    t.boolean "is_read"
    t.boolean "is_add", default: false
    t.boolean "is_update", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_delete", default: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tourisms", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "answer"
    t.integer "point_id"
    t.integer "values"
    t.integer "traveling_id"
    t.integer "question_id"
  end

  create_table "travelings", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "type_answers", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role_id"
    t.string "full_name"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.text "address"
    t.date "birthday"
    t.string "phone"
    t.boolean "popup"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
