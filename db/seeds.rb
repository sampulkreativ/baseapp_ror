if User.first.blank?
	ActiveRecord::Base.connection.execute("TRUNCATE users RESTART IDENTITY")
	puts 'seeding Users table . . .'
	User.create!([ 
		{ email: 'superadmin@base.com', password: '12345678', password_confirmation: '12345678', role_id: 1, full_name: "superadmin", phone: "0850001112233", birthday: Date.today, address: "lorem ipsum dolor sit amet" }
	])
end

if ModuleLabel.first.blank?
	ActiveRecord::Base.connection.execute("TRUNCATE module_labels RESTART IDENTITY")
	puts 'seeding module labels table . . .'
	ModuleLabel.create!([
		{
			name: 'Master Data', index_module: 1, path_module: "#",
			module_class: "fa fa-table",
			active_class: "open_data_master",
			child: nil
		},
		{
			name: 'Users', index_module: 1, path_module: "users_path",
			module_class: "fa fa-user",
			active_class: "active_user",
			child: 1
		},
		{
			name: 'Setting', index_module: 2, path_module: "#",
			module_class: "fa fa-cogs",
			active_class: "open_setting",
			child: nil
		},
		{
			name: 'Roles', index_module: 2, path_module: "roles_path",
			module_class: "fa fa-gear",
			active_class: "active_listing_role",
			child: 3
		},
		{
			name: 'Modules', index_module: 2, path_module: "module_labels_path",
			module_class: "fa fa-list",
			active_class: "active_listing_module",
			child: 3
		}
	])
end

if ModuleApplication.first.blank?
	ActiveRecord::Base.connection.execute("TRUNCATE module_applications RESTART IDENTITY")
	puts 'seeding module applications table . . .'
	ModuleApplication.create!([
		{
			module_label_id: ModuleLabel.find_by_name("Users").id,
			action_read: "can :read, User;",
			action_create: "can :create, User;",
			action_update: "can :update, User;",
			action_destroy: "can :destroy, User;"
		},
		{
			module_label_id: ModuleLabel.find_by_name("Roles").id,
			action_read: "can :read, Role;",
			action_create: "can :create, Role;",
			action_update: "can :update, Role;",
			action_destroy: "can :destroy, Role;"
		},
		{
			module_label_id: ModuleLabel.find_by_name("Modules").id,
			action_read: "can :read, ModuleLabel;",
			action_create: "can :create, ModuleLabel;",
			action_update: "can :update, ModuleLabel;",
			action_destroy: "can :destroy, ModuleLabel;"
		}
	])
end

# RoleModule
# if RoleModule.first.blank?
if RoleModule.first.blank?
	ActiveRecord::Base.connection.execute("TRUNCATE role_modules RESTART IDENTITY")
	puts 'seeding role_modules table . . .'
	ModuleApplication.all.each do |modapp|
		RoleModule.create!([
			{role_id: 1, module_application_id: modapp.id, is_read: 1, is_add: 1, is_update: 1, is_delete: 1},
			{role_id: 2, module_application_id: modapp.id, is_read: 1, is_add: 1, is_update: 1, is_delete: 1},
			{role_id: 3, module_application_id: modapp.id, is_read: 0, is_add: 0, is_update: 0, is_delete: 0},
		])
	end
end

if TypeAnswer.first.blank?
	ActiveRecord::Base.connection.execute("TRUNCATE role_modules RESTART IDENTITY")
	puts 'seeding type_answer table . . .'
	TypeAnswer.create!([
		{name: "Essay", description: "Tipe essay"},
		{name: "Radiobox", description: "Tipe checkbox"},
		{name: "Multiple Choice", description: "Tipe Multiple Choice"},
=======

ActiveRecord::Base.connection.execute("TRUNCATE module_labels RESTART IDENTITY")
puts 'seeding module labels table . . .'
ModuleLabel.create!([
	{
		name: 'Master Data', index_module: 1, path_module: "#",
		module_class: "fa fa-table",
		active_class: "open_data_master",
		child: nil
	},
	{
		name: 'Users', index_module: 1, path_module: "users_path",
		module_class: "fa fa-user",
		active_class: "active_user",
		child: 1
	},
	{
		name: 'Setting', index_module: 2, path_module: "#",
		module_class: "fa fa-cogs",
		active_class: "open_setting",
		child: nil
	},
	{
		name: 'Roles', index_module: 2, path_module: "roles_path",
		module_class: "fa fa-gear",
		active_class: "active_listing_role",
		child: 3
	},
	{
		name: 'Modules', index_module: 2, path_module: "module_labels_path",
		module_class: "fa fa-list",
		active_class: "active_listing_module",
		child: 3
	}
])

ActiveRecord::Base.connection.execute("TRUNCATE module_applications RESTART IDENTITY")
puts 'seeding module applications table . . .'
ModuleApplication.create!([
	{
		module_label_id: ModuleLabel.find_by_name("Users").id,
		action_read: "can :read, User;",
		action_create: "can :create, User;",
		action_update: "can :update, User;",
		action_destroy: "can :destroy, User;"
	},
	{
		module_label_id: ModuleLabel.find_by_name("Roles").id,
		action_read: "can :read, Role;",
		action_create: "can :create, Role;",
		action_update: "can :update, Role;",
		action_destroy: "can :destroy, Role;"
	},
	{
		module_label_id: ModuleLabel.find_by_name("Modules").id,
		action_read: "can :read, ModuleLabel;",
		action_create: "can :create, ModuleLabel;",
		action_update: "can :update, ModuleLabel;",
		action_destroy: "can :destroy, ModuleLabel;"
	}
])

# RoleModule
# if RoleModule.first.blank?
ActiveRecord::Base.connection.execute("TRUNCATE role_modules RESTART IDENTITY")
puts 'seeding role_modules table . . .'
ModuleApplication.all.each do |modapp|
	RoleModule.create!([
		{role_id: 1, module_application_id: modapp.id, is_read: 1, is_add: 1, is_update: 1, is_delete: 1},
		{role_id: 2, module_application_id: modapp.id, is_read: 1, is_add: 1, is_update: 1, is_delete: 1},
		{role_id: 3, module_application_id: modapp.id, is_read: 0, is_add: 0, is_update: 0, is_delete: 0},
		{role_id: 4, module_application_id: modapp.id, is_read: 0, is_add: 0, is_update: 0, is_delete: 0}
	])
end